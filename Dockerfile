FROM python:3.9

WORKDIR /app

COPY Pipfile* ./
RUN pip install pipenv
RUN pipenv install --deploy --ignore-pipfile
COPY ./be_in_demand/ ./
