from typing import Generator
from urllib.parse import urlencode

import scrapy


from ..consts import xpaths
from ..consts.urls import MSK_SEARCH_URL, SPB_SEARCH_URL
from ..consts.xpaths import VACANCY_CARD_MAPPING, VACANCY_MAPPING
from ..item_loaders import HHVacancyCardLoader
from ..items import HHVacancyItem


class VacanciesSpider(scrapy.Spider):
    """
    Scrappy uses this class to implement HH parsing logic.
    """
    name = "vacancies_spider"
    urls = [
        MSK_SEARCH_URL,
        SPB_SEARCH_URL
    ]

    def __init__(self, search_text: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._search_text = search_text

    def parse(self, response, **kwargs):
        """
        Abstract in base class. So, must be implemented
        """

    def start_requests(self):
        for url in self.urls:
            url_params = urlencode({
                'text': self._search_text
            })
            yield scrapy.Request(
                url=f"{url}&{url_params}",
                callback=self.parse_vacancies_list_first
            )

    def parse_vacancies_list_first(self, response, *args, **kwargs):
        """
        Parses first vacancy list page.
        """
        yield from self.parse_vacancies_list(response, *args, **kwargs)
        yield from self._pagination(response)

    def parse_vacancies_list(self, response, *_, **__):
        """
        Parses vacancy list.
        """
        response.selector.remove_namespaces()
        vacancies_cards_selectors_list = response.xpath(xpaths.VACANCY_CARD)

        yield from self._extract_cards_fields(response, vacancies_cards_selectors_list)

    @staticmethod
    def parse_vacancy(response, *_, **__):
        """
        Parses vacancy page.
        """
        response.selector.remove_namespaces()
        item_loader = response.meta['item_loader']
        item_loader.selector = response.selector
        for field, xpath in VACANCY_MAPPING.items():
            item_loader.add_xpath(field, xpath)

        yield item_loader.load_item()

    def _extract_cards_fields(self, response, vacancies_cards_selectors_list: list) -> Generator:
        """
        Extracts data from cards on vacancies list page.
        """
        for card_selector in vacancies_cards_selectors_list:
            item_loader = HHVacancyCardLoader(HHVacancyItem(), card_selector)
            for field, xpath in VACANCY_CARD_MAPPING.items():
                item_loader.add_xpath(field, xpath)
            vacancy_card_link = card_selector.xpath(xpaths.VACANCY_CARD_LINK).extract_first()
            if vacancy_card_link:
                yield response.follow(
                    vacancy_card_link,
                    callback=self.parse_vacancy,
                    meta={
                        'item_loader': item_loader
                    }
                )

    def _pagination(self, response) -> Generator:
        """
        Generates paginated pages and returns generator.
        """
        last_page_number = response.xpath(xpaths.VACANCY_LIST_LAST_PAGE_NUMBER).extract_first()
        for i in range(1, int(last_page_number)):
            yield response.follow(
                f"{response.request.url}&page={i}",
                callback=self.parse_vacancies_list
            )
