from fastapi_utils.cbv import cbv

from api.app import router

"""
Controller for vacancy CRUD operations.
"""
@cbv(router)
class Vacancy:

    async def get_vacancy(self, vacancy_id: str):
        pass
