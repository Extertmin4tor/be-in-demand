from elasticsearch_dsl import Document, Text, Date, analyzer


html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter="snowball",
    char_filter=["html_strip"]
)


class Vacancy(Document):

    title = Text(analyzer='snowball')
    link = Text()
    company_name = Text(analyzer='snowball')
    company_link = Text()
    pub_date = Date()
    salary = Text()
    company_location = Text()
    description = Text(analyzer=html_strip)
    key_skills = Text(analyzer='snowball')

    class Index:
        name = 'be_in_demand'
