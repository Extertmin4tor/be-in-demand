from scrapy import Item, Field


class HHVacancyItem(Item):
    title = Field()
    link = Field()
    company_name = Field()
    company_link = Field()
    pub_date = Field()
    salary = Field()
    company_location = Field()
    description = Field()
    key_skills = Field()
