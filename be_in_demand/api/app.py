from fastapi import FastAPI
from fastapi_utils.inferring_router import InferringRouter

"""
FastApi entrypoint.
"""
app = FastAPI()
router = InferringRouter()
