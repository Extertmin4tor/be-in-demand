# Vacancy card
VACANCY_CARD = '//div[has-class("vacancy-serp-item")]'

# Vacancy title
VACANCY_CARD_TITLE_BASE = './/div[has-class("vacancy-serp-item__info")]//a'
VACANCY_CARD_TITLE = f'{VACANCY_CARD_TITLE_BASE}/text()'
VACANCY_CARD_LINK = f'{VACANCY_CARD_TITLE_BASE}/@href'

# Vacancy company
VACANCY_CARD_COMPANY_BASE = './/div[has-class("vacancy-serp-item__meta-info")]//a'
VACANCY_CARD_COMPANY_NAME = f'{VACANCY_CARD_COMPANY_BASE}/text()'
VACANCY_CARD_COMPANY_LINK = f'{VACANCY_CARD_COMPANY_BASE}/@href'

# Last page xpath
VACANCY_LIST_LAST_PAGE_NUMBER = '//span[contains(text(), "…")]/a/text()'

# Vacancy salary
VACANCY_SALARY = '//p[@class="vacancy-salary"]/span/text()'
VACANCY_COMPANY_LOCATION = '//div[@class="vacancy-address-text"]/span/text()'
VACANCY_DESCRIPTION = '//div[@class="vacancy-section"][1]'
VACANCY_KEY_SKILLS = '(//span[@data-qa="bloko-tag__text"])/text()'
VACANCY_PUB_DATE = '//p[@class="vacancy-creation-time"]/text()'

# Mappings
VACANCY_CARD_MAPPING = {
    'title': VACANCY_CARD_TITLE,
    'link': VACANCY_CARD_LINK,
    'company_name': VACANCY_CARD_COMPANY_NAME,
    'company_link': VACANCY_CARD_COMPANY_LINK,
}

VACANCY_MAPPING = {
    'salary': VACANCY_SALARY,
    'company_location': VACANCY_COMPANY_LOCATION,
    'description': VACANCY_DESCRIPTION,
    'pub_date': VACANCY_PUB_DATE,
    'key_skills': VACANCY_KEY_SKILLS
}
