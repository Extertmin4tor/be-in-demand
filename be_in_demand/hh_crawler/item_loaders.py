import re
from datetime import datetime

import dateparser
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join


def prepare_date(date_parts: list) -> datetime:
    """
    Remove redundant nbsp tag and convert to datetime.

    :param date_parts: Dirty date
    """
    date_str = re.sub("\xa0", " ", date_parts[1])
    return dateparser.parse(date_str)


def filter_salary(salary_part: str) -> str:
    """
    Remove redundant nbsp tag.

    :param salary_part: Dirty salary
    """
    return re.sub("\xa0", " ", salary_part)


class HHVacancyCardLoader(ItemLoader):

    default_output_processor = TakeFirst()
    pub_date_out = prepare_date
    company_location_out = Join("")

    salary_in = MapCompose(filter_salary)
    salary_out = Join("")

    key_skills_out = Join(" ")
