from elasticsearch_dsl import connections


from common.models import Vacancy


class HhSpiderPipeline:

    collection_name = 'vacancies_items'

    @staticmethod
    def open_spider(_):
        """
        On spider started action.
        """
        connections.create_connection(hosts=['elastic'])
        Vacancy.init()

    @staticmethod
    def close_spider(_):
        """
        On spider finished action.
        """
        connections.remove_connection('default')

    @staticmethod
    def process_item(item, _):
        """
        Action for every item, that was scraped.
        """
        Vacancy(**item).save()
